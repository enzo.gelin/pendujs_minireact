// Chrono pour le score
var start = 0
var end = 0
var diff = 0
var timerID = 0
var timestorage = 0


// Boucle chorno
function chrono() {
    // Corps du chorno
    end = new Date()
    diff = end - start
    diff = new Date(diff)
    var sec = diff.getSeconds()
    var min = diff.getMinutes()
    if (min < 10) {
        min = "0" + min
    }
    if (sec < 10) {
        sec = "0" + sec
    }
    // End Corps du chorno
    
    // Stockage en session (pour le score)
    timestorage = +min * 60+ +sec
    sessionStorage.setItem('time', timestorage);
    // Affichage sur la vue 
    document.getElementById("chronotime").innerHTML = min + ":" + sec
    // Boucle
    timerID = setTimeout(chrono, 1000)
}

// Depart chron
function chronoStart() {
    start = new Date()
    chrono()
}

// Arret chrono
function chronoStop() {
    clearTimeout(timerID) // fonction native
}

// Les deux fonctions exportées
export function chronometre() {
    chronoStart();
}
export function endChronometre() {
    chronoStop();
}
