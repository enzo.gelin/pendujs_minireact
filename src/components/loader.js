var myVar;
// Fonction export load au demarage de la page
export function loaderPage() {
    myVar = setTimeout(showPage, 2000);
}
// Fonction export load losr des changement de page
export function loaderPageBis() {
    cachePage();
    myVar = setTimeout(showPage, 3000);
}

// Affiche page
function showPage() {
    document.getElementById("loader").style.display = "none";
    document.body.style.backgroundColor = "white";
    document.getElementById("bodyGeneral").style.display = "block";
}

// Cache page
function cachePage() {
    document.getElementById("loader").style.display = "block";
    document.body.style.backgroundColor = "#c2eced";
    document.getElementById("bodyGeneral").style.display = "none";
}