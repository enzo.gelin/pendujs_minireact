

export function notif(){
    main();
}

// Vérification
const check = () => {
    if (!('serviceWorker' in navigator)) {
        throw new Error('Service Worker non supporté!')
    }
    if (!('PushManager' in window)) {
        throw new Error('Pas de support d\'API Push!')
    }
}

// Fonction utilisé pour le service worker (sw.js)
const registerServiceWorker = async () => {
    const swRegistration = await navigator.serviceWorker.register('assets/js/sw.js');
    return swRegistration;
}
// Demande des permissions d'envoyer des notifs
const requestNotificationPermission = async () => {
    const permission = await window.Notification.requestPermission();
    // valeurs des permission 'granted', 'default', 'denied'
    if(permission !== 'granted'){
        throw new Error('Autorisation non accordée pour la notification');
    }
}
// Affichage de la notif
const showLocalNotification = (title, body, swRegistration) => {
    const options = {
        body,
        // here you can add more properties like icon, image, vibrate, etc.
    };
    swRegistration.showNotification(title, options);
}
// Fonction principale
const main = async () => {
    check();
    const swRegistration = await registerServiceWorker();
    const permission =  await requestNotificationPermission();
    showLocalNotification('Notification', 'Bienvenue sur le jeu du PENDU en Javascript. Pensez vous être le meilleur ou serez-vous pendu ?', swRegistration);
}