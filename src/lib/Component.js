function Component(){
  constructor(properties) {
    this.properties = properties;
    this.newProps = null;
    this.oldProps = null;
    this.state = {};
    this.prevState;
    this.prevRender = null;
  }

  setState = newState => {
    this.prevState = this.state;
    this.state = newState;
    this.display();
  };

  getState = () => {
    return this.state;
  };

  display = () => {
    if (this.shouldUpdate()){
      this.prevRender = this.render();
      return this.prevRender;
    } 
  };
    

  this.shouldUpdate = function () {
      return JSON.stringify(oldProps) !== JSON.stringify(newProps);
  };

}

