import { MiniReactDOM } from "./lib/MiniReact.js";
import { router, route } from "./Router.js";
import { landingPage } from "./components/landingPage.js";
import { HeaderComponent } from "./components/header.js";
import { MainComponent } from "./components/main.js";
import { ErrorComponent } from "./components/error.js";
import { GameComponent } from "./components/game.js";
import './lib/wordpromise.js';
import { ScoreComponent } from "./components/score.js";


MiniReactDOM.render(landingPage, document.getElementById("root"), {});
MiniReactDOM.render(HeaderComponent, document.getElementById("header"), { router });

// Detecte si l'initialisation de l'arboresence c'est bien effectué
var promise = new Promise(function (resolve, reject) {
    // Récuperation du content
    var contentElement = document.getElementById("content");

    // On vérifie qu'on a bien un content dans le DOM pour permettre au MiniReact de fonctionner
    if (contentElement)
        resolve("contenu trouvé");
    else
        reject(
            Error(
                "contenu non trouvé ! Utiliser Google Chrome !"
            )
        );
});

promise.then(
    function (result) {
        var contentElement = document.getElementById("view");
        console.log(result);
        // Affichage en fonction de la route
        switch (!route ? null : route.getId()) {
            case "home":
                // Si on est sur la route home
                MiniReactDOM.render(MainComponent, contentElement, {
                    interval: 1000
                });
                break;

            case "game":
                // Si on est sur la route jitterclick
                MiniReactDOM.render(GameComponent, contentElement, {
                    interval: 600
                });
                break;
            case "score":
                // Si on est sur la route tableau
                MiniReactDOM.render(ScoreComponent, contentElement, {
                    interval: 600
                });
                break;

            default:
                console.log("oui");
                // Not found
                MiniReactDOM.render(ErrorComponent, contentElement, {});
                break;
        }
    },
    function (err) {
        console.log(err);
    }
);
// Initi score table json
const top = {
    '1': {
        'player': "Joueur",
        'score': "0",
        'data': {
            'tentative': '0',
            'motlenght': '0',
            'time': '0',
            'word': '0'
        }
    },
    '2': {
        'player': "Joueur",
        'score': "0",
        'data': {
            'tentative': '0',
            'motlenght': '0',
            'time': '0',
            'word': '0'
        }
    },
    '3': {
        'player': "Joueur",
        'score': "0",
        'data': {
            'tentative': '0',
            'motlenght': '0',
            'time': '0',
            'word': '0'
        }
    }
}
// Local storage score board
if (sessionStorage.getItem("top")) {
    // on test la variabe des scores pour etre sur
    let testsessionStorage = sessionStorage.getItem("top");
    if (testsessionStorage == "undefined") {
        sessionStorage.setItem('top', JSON.stringify(top));
    }
} else {
    sessionStorage.setItem('top', JSON.stringify(top));

}

function boutonClick() {
    document.getElementById('letters').addEventListener('click', event => {
        alert('erfefe');
    });
}

