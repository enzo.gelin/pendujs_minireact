import { Router, Route } from "./lib/routing.js";

// Routeur ( page , nom , path )
export var router = new Router("mainRouter", [
  new Route("Menu", "home", "/"),
  new Route("Jeu", "game", "/jeu"),
  new Route("Score", "score", "/score")
]);

// route courante recupération
export var route = router.routes.filter(function (r) {
  return r.getPath() === window.location.pathname;
})[0];
