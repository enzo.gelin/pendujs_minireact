import { MiniReact } from "../lib/MiniReact.js";
import { Component } from "../lib/component.js";

export class landingPage extends Component {
  constructor(properties) {
    super(properties);
  }

  render = () => {
    const result = MiniReact.createElement("div", { id: "main" },
      MiniReact.createElement("div", {id: "header"}, ""),
      MiniReact.createElement("div", {id: "content"}, "")
    );
    return result;
  };
}
