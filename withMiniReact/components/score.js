// import { prop_access } from '../lib/fonctionscours.js';
import { MiniReact } from "../lib/MiniReact.js";
import { Component } from "../lib/component.js";
import { prop_access } from '../lib/fonctionscours.js';
import { generateTableScore } from "../lib/tablescore.js";



export class ScoreComponent extends Component {
    constructor(properties) {
      super(properties);
    }


    render = () => {
        const result = 
        MiniReact.createElement(
            'div',
            {class: 'col-12'},
            MiniReact.createElement(
                'div',
                {id: 'scorestable', class: 'test'},
                generateTableScore()

            ),
            MiniReact.createElement(
                'div',
                {class: 'col-12'},
            )
        );
        return result;
    }
}