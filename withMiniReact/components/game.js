import { type_check, prop_access } from "../lib/fonctionscours.js";
import { MiniReact } from "../lib/MiniReact.js";
import { Component } from "../lib/component.js";
import { ButtonComponent } from "./button.js";
import { generateTable } from "../lib/tablegenerate.js";
import { decipher } from "../lib/secret.js";
import { saveScore } from "../lib/savescore.js";

export class GameComponent extends Component {
  // Boutton composant
  myBtn = new ButtonComponent({
    onClick: () => document.location.reload(true)
  }).render();

  constructor(properties) {
    super(properties);

    // Vérification des proprietes du composant
    if (this.specs) type_check(properties, this.specs);

    // Mise en place du decompte
    this.timer = setInterval(this.tick, 1000);
    this.temps_total = properties.interval;
    this.temps = 0;
  }
  
  // Affichage
  render = () => {
    const result = 
    MiniReact.createElement(
      "div",
      { class: "container text-center" },
        MiniReact.createElement(
          "div",
          { class: "row"},
          MiniReact.createElement(
            "div",
            { class: "col-12 mt-5" },
              MiniReact.createElement(
                "div",
                { id: "letters" },
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "A" }, 'A'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "B" }, 'B'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "C" }, 'C'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "D" }, 'D'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "E" }, 'E'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "F" }, 'F'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "G" }, 'G'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "H" }, 'H'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "I" }, 'I'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "J" }, 'J'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "K" }, 'K'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "L" }, 'L'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "M" }, 'M'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "N" }, 'N'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "O" }, 'O'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "P" }, 'P'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "Q" }, 'Q'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "R" }, 'R'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "S" }, 'S'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "T" }, 'T'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "U" }, 'U'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "V" }, 'V'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "W" }, 'W'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "X" }, 'X'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark mr-1" , id: "Y" }, 'Y'
                  ),
                  MiniReact.createElement(
                    "button",
                    { class: "letter btn btn-outline-dark" , id: "Z" }, 'Z'
                  )
              )
          ),
          MiniReact.createElement(
            "div",
            { class: "pendupict",name : 'pendupict', id: 'pendupict'}
          )
        ),
        MiniReact.createElement(
          "div",
          { class: "col-md-4 offset-md-4" },
            MiniReact.createElement(
              "div",
              { id: "placeletters" },
              
            )
        ),
        MiniReact.createElement(
          "div",
          { class: "col-md-4 offset-md-4" },
            generateTable()
        ),
        MiniReact.createElement(
          "div",
          { class: "col-12" },
            MiniReact.createElement(
              "div",
              { id: "letterutilise" ,class: "text-center text-danger"},'Lettre déjà utilisée'
            ),
            MiniReact.createElement(
              "div",
              { id: "lost" ,class: "text-center text-danger"},'Vous avez perdu'
            )
        ),
        MiniReact.createElement(
          "div",
          { class: "col-12 mt-5" },
            MiniReact.createElement(
              "h1",
              { id: "chronotime" ,class: "text-center"} , `Temps : ${this.state.time != null ? this.state.time : " 0"}`
            )
        ),
        MiniReact.createElement(
          "div",
          { class: "col-12 mt-5" },
            MiniReact.createElement(
              "h1",
              { id: "scorefinal" ,class: "text-center"}
            )
        ),
        MiniReact.createElement(
          "div",
          { class: "col-12 mt-5" },
            MiniReact.createElement(
              "div",
              { id: "resetpartie" },
                MiniReact.createElement(
                  "button",
                  { id: "" , class : "btn btn-outline-danger btn-lg btn-block" },'Relancer une partie (avec reload de la page)'
                    
                ),
                MiniReact.createElement(
                  "button",
                  { id: "reloadnoreload" , class: "btn btn-outline-success btn-lg btn-block" },"Relancer une partie (sans reload de la page)"
                    
                )
            )
        )
    );
    return result;
  };

 

  // Decompteur
  tick = () => {
    if (this.state.time == null) this.setListener();
    if (this.state.time >= this.temps_total) this.stopCount();
    else this.setState({ time: this.temps++ });
    sessionStorage.setItem('time', this.temps);
    
  };


  stopCount = () => {
    
  };

  // On click
  setListener = () => {
    var element = document.getElementById("letters");
    console.log('test')
    element.onclick = this.counter;
    window.onkeydown = this.counter;
  };

  counter = e => {
    console.log(e.key)
    verifWord(e.key)
  };

  // Modèle des propriété
  specs = {
    properties: {
      interval: { type: "number" }
    }
  };
}


//Lettre incorrecte et correctes
var letterDis = [];
var letterGood = [];

export function boutonClick() {
    document.getElementById('letters').addEventListener('click', event => {
        verifWord(event.path[0].id);
    });
}


//Retourne valeur unique dans un Array
function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

// Affichage message divers time
function displayTime(id) {
    document.getElementById(id).style.display = "block";
    setTimeout(function () {
        document.getElementById(id).style.display = "none";
    }, 2000)
}

function verifWord(keyselect) {
    var keyselect = keyselect.toLowerCase();
    var unique = letterDis.filter(onlyUnique);
    // Action keydown active sur page1 seuelemtn
    if (unique.length < 12) {
        //Recupere le mot en session
        let wordE = sessionStorage.getItem("word");
        let word = decipher(wordE);
        // Transformation du mot en array
        let arrayWord = word.split('');
        // Alphabet en array pour verifier la saisie si la touche est un lettre 
        let alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
        // Si la lettre tapée n'appartient pas au tableau 
        if (arrayWord.includes(keyselect) == false && letterDis.includes(keyselect) == false && alphabet.includes(keyselect) == true) {
            // Nombre total de lettre mauvaise
            const count = letterDis.push(keyselect);
            // Dessin du pendu
            const hangmanPicture = document.getElementById('pendupict');
            var img = document.createElement('img');  
             
            if (count < 12) {
                // Perdu update dessin (grace au count)
                // hangmanPicture.style.backgroundImage = 'url(./assets/images/p' + count + '.png)';
                img.src = './assets/images/p' + count + '.png';

            } else {
                // Perdu fin de partie
                // endChronometre();
                hangmanPicture.style.backgroundImage = 'url(./assets/images/perdu.png)';
                document.getElementById('letters').hidden = true;
            }
            imgBlock = document.getElementById('pendupict').appendChild(img);
            // hangmanPicture.style.visibility = "visible";
        } else if (letterDis.includes(keyselect) == true) {
            // Si la lettre est deja utilisée
            displayTime("letterutilise");
        } else if (arrayWord.includes(keyselect) == true) {
            // Si lettre est dans le mot
            generateTable(keyselect);
            // Nombre total de lettre bonne
            const countGood = letterGood.push(keyselect);
            var uniqueWordGood = letterGood.filter(onlyUnique);
            var uniqueWord = arrayWord.filter(onlyUnique);

            if (uniqueWord.length <= uniqueWordGood.length) {
                // Photo win
                const hangmanPicture = document.getElementById('pendupict');               
                hangmanPicture.src = './assets/images/win.png';
                document.getElementById("resetpartie").style.display = "block";
                // Eteint chrono
                // endChronometre();
                // Sauvegarde le score
                saveScore(unique.length, uniqueWord.length);
            }

        }
        // Désactivation du bouton sur la vue 
        if (alphabet.includes(keyselect) == true) {
            document.getElementById(keyselect.toUpperCase()).disabled = true;
        }
    } else if (unique.length == 12) {
        // Partie perdu
        displayTime("lost");
    }
}

export function resetValH() {
    // Loader
    loaderPageBis();
    //Reset variables
    letterDis = [];
    letterGood = [];
    // Reset disabled
    let alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');
    alphabet.forEach(element => {
        document.getElementById(element.toUpperCase()).disabled = false;
        ;
    });
    // Reset picture
    const hangmanPicture = document.getElementById('pendupict');
    hangmanPicture.style.backgroundImage = 'url(./assets/images/white.png)';
    // Hide boutons
    // document.getElementById("resetpartie").style.display = "none";
}

