import { prop_access } from "../lib/fonctionscours.js";
import { MiniReact } from "../lib/MiniReact.js";
import { Component } from "../lib/component.js";

export class HeaderComponent extends Component {
    constructor(properties) {
        super(properties);
        this.headerTitle = " le jeu du pendu ".snake_case();
        this.routes = prop_access(properties.router, "routes");
        this.selectedLink = window.location.pathname;
    }

    // Fonction de rendu
    render = () => {
        // Recup
        console.log(this.selectedLink);

        // Construction des liens
        var routeHome = this.routes.filter(function (r) {
            return r.getId() === "home";
        })[0];
        var routeScore = this.routes.filter(function (r) {
            return r.getId() === "score";
        })[0];
        var routeGame = this.routes.filter(function (r) {
            return r.getId() === "game";
        })[0];

        // Creation de la base de la vue
        const result = MiniReact.createElement("div", { id: "bodyGeneral" },
            MiniReact.createElement("nav", { class: "navbar navbar-dark fixed-top bg-purple flex-md-nowrap p-0 shadow" },
                MiniReact.createElement("a", { class: "navbar-brand col-sm-3 col-md-2 mr-0" }, `${this.headerTitle}`),
                MiniReact.createElement("p", { class: "text-light mr-5 mt-2", id: "pseudo" }, ``),
                MiniReact.createElement("button", { class: "btn btn-danger", id: "accountcancel" }, `Déconnexion`)
            ),
            MiniReact.createElement("div", { class: "container-fluid" },
                MiniReact.createElement("div", { class: "row" },
                    MiniReact.createElement("nav", { class: "col-md-2 d-none d-md-block bg-light sidebar" },
                        MiniReact.createElement("div", { class: "sidebar-sticky" },
                            MiniReact.createElement("ul", { class: "nav flex-column", id: "list" },
                                MiniReact.createElement("li", { class: "nav-item" },
                                    MiniReact.createElement("a", { class: "nav-link", id: "navhome", href: "." + routeHome.getPath(), style: this.selectedLink === routeHome.getPath() ? "font-weight: bold" : "" }, routeHome.getName()),
                                ),
                                MiniReact.createElement("li", { class: "nav-item" },
                                    MiniReact.createElement("a", { class: "nav-link", id: "navpage1", href: "." + routeGame.getPath(), style: this.selectedLink === routeGame.getPath() ? "font-weight: bold" : "" }, routeGame.getName()),
                                ),
                                MiniReact.createElement("li", { class: "nav-item" },
                                    MiniReact.createElement("a", { class: "nav-link", id: "navpage2", href: "." + routeScore.getPath(), style: this.selectedLink === routeScore.getPath() ? "font-weight: bold" : "" }, routeScore.getName()),
                                ),
                                MiniReact.createElement("li", { class: "nav-item" },
                                    MiniReact.createElement("p", { class: "nav-link", id: "navpage3" })

                                )
                            )
                        )
                    ),
                    MiniReact.createElement("main", { class: "col-md-9 ml-sm-auto col-lg-10 px-4 mt-5" },
                        MiniReact.createElement("div", { id: "view" })
                    )
                )
            )
        );

        return result;
    };
}

debugger
// Get level batterie easy
navigator.getBattery().then(function (battery) {
    var level = battery.level;
    var level = Math.round(level * 100);
    var battTxt = document.getElementById('navpage3');
    var textNode = document.createTextNode(level+' % de batterie')
    battTxt.appendChild(textNode)
    console.log(level)
});
