import { type_check } from "../lib/fonctionscours.js";
import { MiniReact } from "../lib/MiniReact.js";
import { Component } from "../lib/component.js";

export class MainComponent extends Component {
  constructor(properties) {
    super(properties);
    setInterval(this.tick, properties.interval);
    if (this.specs) type_check(properties, this.specs);
  }

  render = () => {
    const result = MiniReact.createElement(
      "div",
      { class: "container text-center" },
      MiniReact.createElement(
        "h1",
        { id: "horloge" },
        `${
          this.state.ticker == null
            ? new Date().toLocaleTimeString()
            : this.state.ticker
        }`
      )
    );

    return result;
  };

  tick = () => {
    const time = new Date().toLocaleTimeString();
    this.setState({ ticker: time });
  };

  specs = {
    properties: {
      interval: { type: "number" }
    }
  };
}
