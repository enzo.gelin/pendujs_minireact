import { cipher } from './secret.js';


function randomIntFromInterval(min, max) {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min) * 1000;
}

// Recup via api d'un mot random (en anglais)
const getWord = async function () {
    const response = await fetch("https://random-word-api.herokuapp.com/word?number=1");
    const data = await response.json();
    return data;
};

// Mapping
const mapping = async function () {
    const results = await Promise.all([getWord()]);
    const word = results[0];
    // variable de test
    sessionStorage.setItem('wordEXTRAASUPP', word);
    // cryptage du mot
    const wordencrypted = cipher(word);
    // Enregistrement du mot encrypé dans les session
    sessionStorage.removeItem('word');
    sessionStorage.setItem('word', wordencrypted);

    return word.map((word) => {
        return word;
    });
};

// Timer
const timer = async function () {
    setTimeout(() => {}, 6000);
};

Promise.race([mapping(), timer()])
    .then(() =>  console.error("Merge ok"))
    .catch(() => console.error("Timeout"));

export function wordReload(){
    Promise.race([mapping(), timer()])
    .then(() =>  console.error("Merge ok"))
    .catch(() => console.error("Timeout"));
}