import { prop_access } from '../lib/fonctionscours.js';
import { decipher } from './secret.js';

export function saveScore(nbreTentative, longWord) {
    // Recuperation mot crypté
    var wordE = sessionStorage.getItem('word');
    // Decryptage
    var wordR = decipher(wordE);
    // Recuperation temps et user pseudo
    var timeT = sessionStorage.getItem('time');
    var user = sessionStorage.getItem('person');
    // Calcul du score (le calcul n'as pas de logique)
    let scorePlayer = +50000 - (+timeT * +212) + (+longWord * +48) - (+nbreTentative * 67);

    //Affichage su score

    // Variable de session pour les tests
    sessionStorage.setItem('tentative', nbreTentative);
    sessionStorage.setItem('motlenght', longWord);
    sessionStorage.setItem('score', scorePlayer);

    // Recuperation json array SCORE
    var top = JSON.parse(sessionStorage.getItem('top'));

    // Divers variable pour l'affichage correct du score
    let thirdScore = prop_access(top, "3.score");
    let secondPlayer = prop_access(top, "2.player");
    let secondScore = prop_access(top, "2.score");
    let secondTentative = prop_access(top, "2.data.tentative");
    let secondMotL = prop_access(top, "2.data.motlenght");
    let secondTime = prop_access(top, "2.data.time");
    let secondWord = prop_access(top, "2.data.word");
    let firstPlayer = prop_access(top, "1.player");
    let firstScore = prop_access(top, "1.score");
    let firstTentative = prop_access(top, "1.data.tentative");
    let firstMotL = prop_access(top, "1.data.motlenght");
    let firstTime = prop_access(top, "1.data.time");
    let firstWord = prop_access(top, "1.data.word");
    debugger;
    // Comparaison du score fait aux 3 meilleurs scores
    if (scorePlayer > thirdScore) {
        if (scorePlayer > secondScore) {
            if (scorePlayer > firstScore) {
                var new_obj = {
                    ...top,
                    1: {
                        player: user, score: scorePlayer, data:
                            { tentative: nbreTentative, motlenght: longWord, time: timeT, word: wordR }
                    },
                    2: {
                        player: firstPlayer, score: firstScore, data:
                            { tentative: firstTentative, motlenght: firstMotL, time: firstTime, word: firstWord }
                    },
                    3: {
                        player: secondPlayer, score: secondScore, data:
                            { tentative: secondTentative, motlenght: secondMotL, time: secondTime, word: secondWord }
                    }
                }
            } else {
                var new_obj = {
                    ...top,
                    2: {
                        player: user, score: scorePlayer, data:
                            { tentative: nbreTentative, motlenght: longWord, time: timeT, word: wordR }
                    },
                    3: {
                        player: secondPlayer, score: secondScore, data:
                            { tentative: secondTentative, motlenght: secondMotL, time: secondTime, word: secondWord }
                    }
                }
            }
        } else {
            var new_obj = {
                ...top,
                3: {
                    player: user, score: scorePlayer,
                    data: { tentative: nbreTentative, motlenght: longWord, time: timeT, word: wordR }
                }
            }
        }
    }
    // Enregistrement du tableau
    sessionStorage.setItem('top', JSON.stringify(new_obj));
}