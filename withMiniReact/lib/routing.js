export class Route {
  constructor(name, id, path) {
    this.name = name;
    this.id = id;
    this.path = path;
  }

  getName = () => {
    return this.name;
  };

  getId = () => {
    return this.id;
  };

  getPath = () => {
    return this.path;
  };


  getLinkElement = () => {
    return this.path;
  };
}

export class Router {
  constructor(name, routes) {
    this.name = name;
    this.routes = routes;
  }

  getName = () => {
    return this.name;
  };

  getRoutes = () => {
    return this.routes;
  };
}
